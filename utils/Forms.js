import {post} from "./Ajax.js";

(function ($) {
    $.fn.button = function (action) {
        if (this.length > 0) {
            this.each(function () {
                if (action === 'loading' && $(this).data('loading-text')) {
                    $(this).data('original-text', $(this).html()).html($(this).data('loading-text')).prop('disabled', true);
                } else if (action === 'reset' && $(this).data('original-text')) {
                    $(this).html($(this).data('original-text')).prop('disabled', false);
                }
            });
        }
    };
}(jQuery));

function formLogin() {
    $('#loginForm').on("submit", function (event) {
        event.preventDefault();
        var loginBtn = $('#loginBtn');
        loginBtn.button('loading');
        var args = {
            email: $('#loginEmail').val(),
            password: $('#loginPassword').val()
        };
        var request = post('api/login', args);
        request.then(function (response) {
            var obj = JSON.parse(response);
            var err = document.getElementById("errorAjax");
            err.hidden = true;
            var errTxt = $('#errorAjaxTxt');

            if (obj.state == "1") {
                loginBtn.button('reset');
                errTxt.text(obj.error);
                err.hidden = false;
            } else if (obj.state == "0") {
                var token = obj.token;
                loginBtn.button('reset');
                localStorage.setItem('token', token);
                window.location.replace(obj.redirect);
            } else {
                loginBtn.button('reset');
                errTxt.text("Une erreur interne est survenue");
                err.hidden = false;
            }
        });
    });
}

function formRegister() {
    $('#registerForm').on("submit", function (event) {
        event.preventDefault();
        var registerBtn = $('#registerBtn');
        registerBtn.button('loading');
        var args = {
            firstName: $("#registerFirstName").val(),
            lastName: $("#registerName").val(),
            email: $('#registerEmail').val(),
            password: $('#registerPassword').val(),
            passwordConf: $('#registerPasswordConf').val(),
            captcha: grecaptcha.getResponse()
        }
        var request = post('api/register', args);
        request.then(function (response) {
            var obj = JSON.parse(response);
            var err = document.getElementById("errorAjax");
            err.hidden = true;
            var errTxt = $('#errorAjaxTxt');
            if (obj.state == "1") {
                registerBtn.button('reset');
                errTxt.text(obj.error);
                err.hidden = false;
            } else if (obj.state == 0) {
                console.log("state0");
                registerBtn.button('reset');
                localStorage.setItem('token', obj.token);
                window.location.replace(obj.redirect);
            } else {
                registerBtn.button('reset');
                errTxt.text("Une erreur interne est survenue");
                err.hidden = false;
            }
        })
    });
}

function registerFrontForms() {
    formLogin();
    formRegister();
}

export {registerFrontForms}