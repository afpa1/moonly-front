const apiUrl = "http://api.localhost/index.php/";
const siteUrl = "http://localhost/";

/**
 * @param target
 * @see Routes in symfony
 * @param args
 * @example post('login', { email : "plop@emal.fr" , password : "4dM1n"})
 */
async function post(target, args) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: apiUrl+target,
            type: 'POST',
            data: args,
            dataType: 'text',
            async: false,
            timeout: 30000,
            success: (response) => {
                resolve(response);
            },
            error: (response) => {
                reject(response);
            }
        })
    })
}

export {post}