import { getVitrine, getLogin } from "../controller/Vitrine.js";
import { mainApp } from "../controller/App.js";
export default function Router(app) {
    return {
        route: route(app)
    };
}

function route(app) {
    Path.map("#/").to(function () {
        getVitrine(app)
    });
    Path.map("#/login").to(function (){
        getLogin(app);
    });
    Path.map("#/app").to(function(){
        mainApp(app);
    });
    /*Path.map("#/plop").to(function () {
        alert("Plop! ");
    }).enter(
        function () {
            $('#page').innerHTML = '';
        }
    );
    Path.map("#/plop/:id").to(function () {
        alert("Plop! " + this.params['id']);
    });*/
    Path.rescue(function () {
        alert("404: Route Not Found");
        console.log("Err. route");
    });

    Path.root("#/");

    Path.listen();

}
