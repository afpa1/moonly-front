import {isLogged} from "../models/User_model.js";
import navbar from "../templates/app/navbar.js";
import leftbar from "../templates/app/leftbar.js";
import pagetop from "../templates/app/pagetop.js";
import dashboard from "../templates/app/dashboard.js";
import foot from "../templates/app/foot.js";


async function getResponse(app) {
    if (localStorage.getItem('token') != null) {
        isLogged().then(
            data => displayPage(JSON.parse(data), app)
        )
    } else {
        let log = {
            "state": false
        };
       displayPage(log, app)
    }

}

async function mainApp(app) {
        await getResponse(app)
}

async function displayPage(isLog, app) {
    if (isLog.state) {
        loadCss(app)
        /*app.append(navbar().view,
            leftbar().view
            )*/
        let html = "";
        html = html + navbar().view;
        html = html + leftbar().view;
        html = html + pagetop().view;
        html = html + dashboard().view;
        html = html + foot().view;
        app.append(html)
        //console.log(navbar().view)
        /*app.append(leftbar().view)
        app.append(pagetop().view)
        app.append(dashboard().view)
        app.append(foot().view)*/
    } else {
        window.location.replace('/#/login');
    }

}


function loadCss(app) {
    app.empty();
    app.removeClass();
    app.append(
        `
 <link href="/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
    <script>
        var theme = localStorage.getItem("theme");
        if (theme != null) {
            $('body').append('<link href="' + theme + '" rel="stylesheet" type="text/css" class="theme">');
        } else {
            $('body').append('<link href="/assets/css/light.css" rel="stylesheet" type="text/css" class="theme">');
        }
    </script>
    <link rel="shortcut icon" href="/assets/img/favicon.png">

    <link href="/assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin="">
          <script src="/assets/js/popper.min.js"></script>

    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
            integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
            crossorigin=""></script>

    <link rel="stylesheet" href="https://themesbrand.com/veltrix/layouts/plugins/chartist/css/chartist.min.css">
    <script src="https://cdn.jsdelivr.net/chartist.js/latest/chartist.min.js"></script>

`
    )
}

export {mainApp}
