import nav from "../templates/front/nav.js";
import masthead from "../templates/front/masthead.js";
import features from "../templates/front/features.js";
import price from "../templates/front/price.js";
import talking from "../templates/front/talking.js";
import about from "../templates/front/about.js";
import foot from "../templates/front/foot.js";
import end from "../templates/front/end.js";
import login from "../templates/front/login.js";
import {getJson} from "../models/Home_model.js";
import {registerFrontForms} from "../utils/Forms.js";
import {isLogged} from "../models/User_model.js";

async function isLog(app, page) {
    isLogged().then(
        data => page === "vitrine" ? getConfig(app, JSON.parse(data)) : loadLogin(app, JSON.parse(data))
    )
}

async function getConfig(app, isLog) {
    getJson().then(
        data => loadVitrine(app, isLog, JSON.parse(data))
    )
}

async function loadLogin(app, isLog) {
    loadCss(app)
    app.addClass("h-100");
    app.append(nav(isLog.state).view);
    app.append(login().view);
    app.append(end().view);
    loadSlide();
    registerFrontForms()
}

async function loadVitrine(app, isLog, config) {
    loadCss(app)
    app.addClass("h-100");
    app.append(nav(isLog.state).view);
    app.append(masthead(config).view);
    app.append(features(config).view);
    app.append(price().view);
    app.append(talking(config).view);
    app.append(about(config).view);
    app.append(foot().view);
    app.append(end().view);
    loadSlide();
    registerFrontForms();
}

async function getVitrine(app) {

    if (localStorage.getItem('token') != null) {
        isLog(app, "vitrine");
    } else {
        let log = {
            "state": false
        };
        getConfig(app, log);
    }
}

async function getLogin(app) {
    if (localStorage.getItem('token') != null) {
        isLog(app, "login");
    } else {
        let log = [{
            'state': false
        }];
        loadLogin(app, log);
    }
}


function loadCss(app) {
    app.empty();
    app.removeClass();
    app.append(
        `
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css"
          href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <script src="https://kit.fontawesome.com/d85cdf4ce3.js" crossorigin="anonymous"></script>
    

`
    )
}

function loadSlide() {

    $('.Count').each(function () {
        var $this = $(this);
        jQuery({Counter: 0}).animate({Counter: $this.text()}, {
            duration: 3000,
            easing: 'swing',
            step: function () {
                $this.text(Math.ceil(this.Counter));
            }
        });
    });

    $(".value-set").each(function () {
        $(this).stars({value: $(this).data("stars")});
    });

    $('.items').slick({
        dots: true,
        infinite: true,
        speed: 800,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });
    $(function () {
        $(".slide").click(function (event) {

            event.preventDefault();
            if (location.hash !== "#/") {
                window.location.hash = "#/";
                var anchor = $(this.hash);
                var slice = this.hash.slice(1);
                getVitrine($('#page'));
            }
            var anchor = $(this.hash);
            anchor = anchor.length ? anchor : $("[name=" + this.hash.slice(1) + "]");
            if (anchor.length) {
                $("html, body").animate({scrollTop: anchor.offset().top}, 1500);

            }


        });

        function scrollTo(anchor, slice) {
            console.log('called')
            anchor = anchor.length ? anchor : $("[name=" + slice + "]");
            if (anchor.length) {
                $("html, body").animate({scrollTop: anchor.offset().top}, 1500);

            }
        }

    });
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#scroll').fadeIn();
            } else {
                $('#scroll').fadeOut();
            }
        });
        $('#scroll').click(function () {
            event.preventDefault()
            $("html, body").animate({scrollTop: 0}, 600);
            return false;
        });
    });
}

export {getVitrine, getLogin};
