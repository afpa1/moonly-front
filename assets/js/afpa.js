$(document).ready(function () {
    var link = $("link.theme");
    var theme = localStorage.getItem("theme");
    if (theme!= null){
        link.attr("href", theme);
    }
    $('#btn-theme').click(function () {
        event.preventDefault();
        var themed = link.attr("href");
        var light = "/assets/css/light.css";
        var black = "/assets/css/dark.css"

        if (themed == light) {
            link.attr("href", black);
        } else {
            link.attr("href", light);
        }
        localStorage.setItem("theme", link.attr("href"));
    });
});
