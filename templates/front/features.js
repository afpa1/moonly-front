var template = `<section class="page-section " id="features">
    <div class="container h-100">
        <h2 class="text-center mt-4">Fonctionnalités</h2>
        <hr class="divider light my-4">
        <div class="row " id="featureBox">
                %features%
        </div>
    </div>
</section>`;

export default function (config) {
    return {
        view: loadFeatures(config)
    };
}

var features = `    <div class="%class%">
                        <div class="card border-light" style="">
                            <img class="card-img-top" src="%img%" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="card-title">%title%</h5>
                                <p class="card-text">%sub%</p>
                            </div>
                        </div>
                    </div>
`;

function loadFeatures(config) {
    let x;
    var result = "";
    for (x in config.features) {
        let feature = config.features[x];
        let featureBox = features;
        if (x < 3) {
            featureBox = featureBox.replace("%class%", "col-md-4 mb-4");
        } else {
            featureBox = featureBox.replace("%class%", "col-md-4 mb-4 d-none d-md-block");
        }
        featureBox = featureBox.replace("%img%", feature.img);
        featureBox = featureBox.replace("%title%", feature.title);
        featureBox = featureBox.replace("%sub%", feature.sub);
        result = result+featureBox;
    }
    return template.replace(" %features%", result);
}