var template = `<section class="page-section" id="talking">
    <div class="container-fluid">
        <h2 class="text-center mt-4">Ils parlent de nous </h2>
        <hr class="divider light my-4">
        <div class="row items text-black-50 " id="ratingBlock">
            %talking%
        </div>
    </div>

</section>
`;

export default function (config) {
    return {
      view : generate(config)
    };
}

const ratingsTemplate = `
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6"><img src="%img%" width="50%" class="rounded-circle">
            </div>
            <div class="col-sm-5 ">
                <div class="value-set"  data-stars="%star%"></div>
                <br>
                <h5 class="card-title">%nom% %prenom%</h5>
            </div>
        </div>
        <p class="card-text">"%comments%"</p>   
    </div>
</div>`;

function generate(config) {
    var ratingsTxt = "";
    let x;
    for (x in config.ratings){
        let rating = config.ratings[x];
        let ratingBox = ratingsTemplate;
        ratingBox = ratingBox.replace("%img%", rating.img);
        ratingBox = ratingBox.replace("%star%", rating.stars);
        ratingBox = ratingBox.replace("%nom%", rating.nom);
        ratingBox = ratingBox.replace("%prenom%", rating.prenom);
        ratingBox = ratingBox.replace("%comments%", rating.comments);
        ratingsTxt = ratingsTxt + ratingBox;
    }
    return template.replace("%talking%", ratingsTxt);
}


