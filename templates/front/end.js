var template = `<script src="/assets/js/afpa.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/holder.min.js"></script>
<script>
    Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
    });
</script>
<script type="text/javascript" src="/assets/vendor/stars/js/stars.min.js"></script>
`;
export default function () {
    return{
        view: template
    };
}
