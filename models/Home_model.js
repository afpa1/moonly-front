import {post} from "../utils/Ajax.js"

async function getJson() {
    var posting = post( 'api/config', {} );
    var json = posting.then(function (response) {
        return  response;
    }).catch(function (response) {
        return  response;
    })
    return json;
}

export { getJson}
