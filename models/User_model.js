import {post} from "../utils/Ajax.js";

function isLogged(){
  var token = localStorage.getItem('token');
  if (token != null) {
    return post('api/token', {token : token});
  }else{
    return null;
  }

}

function login(email, password){
    //todo here we ll prepare our API call
}

function register(firstName, lastName, email, password, passwordConf, captcha){
  //todo here we ll prepare our API call
}

export { isLogged }
